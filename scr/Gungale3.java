package myrobot;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

/**
 *
 * @author italo ian
 */
public class Gungale3 extends AdvancedRobot {

    private String targ; // que robô mira
    private short prevE;//energia anterior do robô que estamos atirando
    private byte dir = 1;// Direção para mover
    private byte moveDirection = 1;
    private byte spins = 0; // contador de giros do robô

    @Override
    public void run() {
        setColors(Color.BLACK, Color.BLACK, Color.BLACK); // gungale black
        setAdjustGunForRobotTurn(true); // quando o robô se virar, ajuste a arma na direção oposta
        setAdjustRadarForGunTurn(true); // quando a arma gira, ajuste o radar no dir oposto
        while (true) { // para bloqueio de radar
            turnRadarLeftRadians(1); // fazer o radar girar continuamentepara a esquerda
            setTurnRadarRight(360); // Roda o radar em 360º
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        if (targ == null || spins > 6) {
            targ = e.getName();
        }
        circularMove(e);

        if (e.getName().equals(targ)) { // se o robô verificado é nosso alvo
            spins = 0; // resetar contador
            if ((prevE < (prevE = (short) e.getEnergy())) && Math.random() > .85) {
                dir *= -1;
            }
            // Mova a arma na direção do inimigo
            setTurnGunRightRadians(Utils.normalRelativeAngle((getHeadingRadians() + e.getBearingRadians()) - getGunHeadingRadians()));
            // o dado é dado de acordo a distancia em pixels
            if (e.getDistance() < 200) {
                setFire(3.5);
            } else if (e.getDistance() < 300) {
                setFire(2.0);
            } else if (e.getDistance() < 800) {
                setFire(1.5);
            } else {
                setFire(0.5);
            }

            // Calcula o angulo para o radar retornar e trava o radar
            double radarTurn = getHeadingRadians() + e.getBearingRadians() - getRadarHeadingRadians();
            setTurnRadarRightRadians(2 * Utils.normalRelativeAngle(radarTurn));
        } else if (targ != null) {
            spins++;
        }

    }

    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        /**
         * Se for atingido por uma bala, pegamos o atirador e setamos como alvo
         */
        targ = e.getName();
    }
    // Implementação da estratégia de movimento circular com fuga de paredes

    public void circularMove(ScannedRobotEvent e) {
        // Sempre se posiciona contra o nosso inimigo, virando um pouco para ele
        setTurnRight(normalizeBearing(e.getBearing() + 90 - (15 * moveDirection)));

        // mudar de direção se paramos (também se afasta da parede se estiver muito perto)
        if (getVelocity() == 0) {
            setMaxVelocity(10); // muda a velocidade para 10
            moveDirection *= -1;
            setAhead(10000 * moveDirection);
        }
    }
    // normaliza um rolamento entre +180 e -180

    double normalizeBearing(double angle) {
        while (angle > 180) {
            angle -= 360;
        }
        while (angle < -180) {
            angle += 360;
        }
        return angle;
    }

}
