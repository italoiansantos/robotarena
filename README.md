## **Robot Arena Robocode**

## Gungale
o gungale faz uma varedura de 360º até encontrar um alvo, após encontrar o alvo ou ser alvejado por algum inimigo(isso faz com que adiante o processo de varredura), ele começa a atirar(quanto mais perto, mais forte o dando, vise versa) e faz movimentos circulares após detectar seu alvo... bastante efeivo em x1 e sobrevivencia.   

### Pontos Fortes
- Movimentação circular
- efetivo contra diversos inimigos
- economia de energia
### Pontos Fracos
- A distancia não é tão efetivo.

Desenvolvido para a Solutis Robot Arena por Italo Ian Cruz dos Santos.



